INTRODUCTION
------------
This module created for extending a list of variables available in author-pane.tpl.php file.
Module developed specially for site www.agroscience.com.ua.

REQUIREMENTS
------------
This module tested and work properly with Author Pane 7.x-2.0

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
 * After enabling this module you can use next extra variables in author-pane.tpl.php file:
    -> $user_pic - renderable array with User picture based on field_user_avatar field.
    -> $comment_count - amount of comments by every user.
    -> $location - string with comma-separated user location e.g.: "Україна, Київська обл., м. Київ".
    -> $role - string of user's roles on site.

CONFIGURATION
-------------
 * You should copy author-pane.tpl.php file from 'author_pane' module folder to your theme folder
   and than customize it.

MAINTAINER
-----------
 * Taras Kyryliuk (JGoover) - https://drupal.org/user/2901135